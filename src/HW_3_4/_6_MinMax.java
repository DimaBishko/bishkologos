package HW_3_4;

public class _6_MinMax {
    public static void main(String[] args) {
        int[] RandomNumbers = {1, 2, 3, -500, 4, 0, 3500, 524, -376, -7, -500, -500, 1000};
        int max = 0;
        int min = 0;

        for (int i = 0; i < RandomNumbers.length; i++) {

            if (RandomNumbers[i] > max) {
                max = RandomNumbers[i];
            }
            if (RandomNumbers[i] < min) {
                min = RandomNumbers[i];
            }
        }
        System.out.println("Max = " + max + ",   Min = " + min);
    }
}

