package HW_3_4;

// Привіт. В мене відбулась трошки запара з навчанням. Останній семестр, скорочений, і тому сесія проходила вкінці жовтня.
// Так як у мене вечірня форма навчання - вся фігня наклалась на час курсів.
// Зараз вже фініш, почалась переддипломна практика і написання магістерської. Але нова проблемка - у брата Covid.
// Все норм, лише смак і нюх пропав. У мене взагалі жодних симптомів, але я теоретично можу бути розповсюджувачем того.
// Тому вимушений посидіти вдома ще 2-3 тижні.
// Але я не пропав і не забив на курси, згодом з'явлюсь)

import java.util.Scanner;
public class _1a_QuadEqua {
    public static void main (String[] args) {
        int a, b, c;
        int D;

        System.out.println("Дано квадратне рівняння:");
        System.out.println("ax^2 + bx + c = 0");
        System.out.println("Введіть (через Space) a, b, c:");

        Scanner scanner = new Scanner(System.in);

        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();
        D = b * b - 4 * a * c;

        if (a == 0) {
            System.out.println("Рівняння розв'язків не має. На нуль ділити не можна... Ну, тіпа, можна, але нічого доброго з того не вийде)");
        }
        else if (D > 0) {
            double x1, x2;
            x1 = (-b - Math.sqrt(D)) / (2 * a);
            x2 = (-b + Math.sqrt(D)) / (2 * a);
            System.out.println("Корені: x1 = " + x1 + ", x2 = " + x2);
        }
        else if (D == 0) {
            int x;
            x = (-b / (2 * a));
            System.out.println("Відповідь: x = " + x);
        }
        else {
            System.out.println("Немає коренів");
        }
    }
}
