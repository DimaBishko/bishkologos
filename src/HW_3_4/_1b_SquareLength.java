package HW_3_4;
import java.util.Scanner;

public class _1b_SquareLength {
    public static void main(String[] args) {
        double r, R, L;

        System.out.println("Введіть радіус кола: ");

        Scanner in = new Scanner(System.in);

        r = in.nextInt();
        R = r * r * Math.PI;
        L = 2 * r * Math.PI;

        if (r > 0) {
            System.out.println("Площа круга = " + R + "; Довжина кола = " + L);
        } else {
            System.out.println("Радіус кола не може бути меншим нуля чи дорівнювати йому!!!");
        }
    }
}

