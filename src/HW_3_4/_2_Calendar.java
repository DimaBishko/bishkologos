package HW_3_4;
import java.util.Scanner;

public class _2_Calendar {
    public static void main (String[] args) {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Введіть порядковий номер дня тижня (1-7): ");

        int Week = scanner.nextInt();
        String action;

        switch(Week) {
            case 1:
                action = "Скинути індз Бандурі";
                break;
            case 2:
                action = "Залік в Москаль";
                break;
            case 3:
                action = "Скинути лаб Цікало";
                break;
            case 4:
                action = "Модуль 2 в Бандури";
                break;
            case 5:
                action = "Захист індз + презентації в Шевчук";
                break;
            case 6:
                action = "Почати нарешті дипломну (спроба n)";
                break;
            case 7:
                action = "Почати нарешті дипломну (спроба n+1)";
                break;
            default:
                action = "Тиждень не має стільки днів (або некоректно введено порядковий номер)";
                break;
        }
        System.out.println(action);
    }
}
