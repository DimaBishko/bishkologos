package HW_3_4;
import java.util.Scanner;

public class _5_CloserNumberTo10 {
    public static void main(String[] args) {
        double a, b;

        System.out.println("введіть 2 числа: ");
        Scanner scanner = new Scanner(System.in);
        a = scanner.nextDouble();
        b = scanner.nextDouble();

        if (Math.abs(a-10) < Math.abs(b-10)){
            System.out.println(a);
        }
        else if (Math.abs(a-10) == Math.abs(b-10)){
            System.out.println("Відстань однакова");
        }
        else {
            System.out.println("Ви ввели некоректне значення");
        }
    }
}
